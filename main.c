#include <stdio.h>
#include <vlc/vlc.h>
#include <errno.h>

int main() {
    printf("hello world\n");
    fprintf(stderr, "hello error\n");
    libvlc_instance_t *libvlc;
    libvlc_media_t *m;
    libvlc_media_player_t *mp;
    char const *vlc_argv[] =
    {
        "--no-plugins-cache"
    };
    int vlc_argc = sizeof(vlc_argv) / sizeof(*vlc_argv);
    libvlc = libvlc_new(vlc_argc, vlc_argv);
    m = libvlc_media_new_path(libvlc, "01.mp3");
    mp = libvlc_media_player_new_from_media(m);
    libvlc_media_release(m);

    libvlc_media_player_release(mp);
    libvlc_release(libvlc);

    return 0;
}
