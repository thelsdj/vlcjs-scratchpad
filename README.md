Instructions for building VLC using emscripten on MacOS Sierra

1. Install emscripten http://kripken.github.io/emscripten-site/docs/getting_started/downloads.html#platform-notes-installation-instructions-portable-sdk and http://kripken.github.io/emscripten-site/docs/getting_started/downloads.html#all-os-installation-instructions-portable-sdk
2. Build 'incoming' emscripten which is needed for getting pthreads to work on latest Firefox Nightly http://kripken.github.io/emscripten-site/docs/building_from_source/building_emscripten_from_source_using_the_sdk.html
3. Install Firefox Nightly
4. Checkout VLC git. (https://git.videolan.org/git/vlc.git)
6. Apply `vlc-emscripten.patch` in vlc root directory: `patch -p1 < /path/to/vlcjs-scratchpad/vlc-emscripten.patch`
7. Follow step-by-step instructions here (not the single command): https://wiki.videolan.org/OSXCompile/ and make the following changes.
8. When you get to bootstrapping third party libraries use `darwin15` instead of `darwin11`
9. Instead of `../extras/package/macosx/configure.sh`, run the following in `build` directory: `emconfigure ../configure --disable-lua --disable-httpd --disable-vlm --disable-addonmanagermodules --disable-avcodec  --enable-merge-ffmpeg --disable-swscale --disable-a52 --disable-x264 --disable-xcb --disable-xvideo --disable-sdl --disable-alsa --disable-macosx --disable-sparkle --host=x86_64-unknown-linux-gnu --build=x86_64-apple-darwin15`
10. Before running make, patch `src/Makefile` to not try to build linux specific code (within `build` dir): `patch -p0 < /path/to/vlcjs-scratchpad/linux-makefile.patch`
11. Also patch `config.h` with `patch -p0 < /path/to/vljs-scratchpad/config-h-fixes.patch`
12. `echo "" > ../src/revision.c`
12. Next instead of just plain 'make' run: `emmake make -j4`
13. Copy `main.c` from this directory into your vlc build directory.
14. Compile test program with `emcc -s USE_PTHREADS=1 -o test.html main.c -I../include -Llib/.libs/ -lvlc -Lsrc/.libs -lvlccore`
15. Open test.html in Firefox Nightly and see what happens (likely an exception at this point, but with any luck you will see in the javascript console a stack trace that is calling into VLC)
